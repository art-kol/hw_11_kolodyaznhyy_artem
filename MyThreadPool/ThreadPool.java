package MyThreadPool;

public interface ThreadPool {
    void start();
    void execute(Runnable runnable);
    void interrupted();
}
