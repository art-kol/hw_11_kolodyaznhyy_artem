package MyThreadPool;

public class MyRunnable implements Runnable{
    @Override
    public void run() {
        try {
            System.out.println("Start Runnable: "+ Thread.currentThread().getId());
            double a = 0;
            for (int i = 0; i < 2000000; i++) {
                a = a + Math.tan(a) + Math.sin(a) + Math.cos(a);
            }
            System.out.println("Finish Runnable: "+ Thread.currentThread().getId());
        } catch (Exception e){
            Thread.currentThread().interrupt();
        }
    }

}
