package MyThreadPool;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        // Дождаться включения visualVM
        Thread.sleep(10000);

        ThreadPool pool = new ScalableThreadPool(10, 20);
        pool.start();
        for (int i = 0; i < 30; i++) {
            pool.execute(new MyRunnable());
        }

        // Для проверки удаления потоков
        Thread.sleep(5000);

        pool.interrupted();


    }
}
