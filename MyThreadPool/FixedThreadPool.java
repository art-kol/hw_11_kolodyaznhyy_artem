package MyThreadPool;

import java.util.concurrent.LinkedBlockingQueue;

public class FixedThreadPool implements ThreadPool {

    private final int quantityThreads;
    private final Workers[] threads;
    private final LinkedBlockingQueue queue;

    public FixedThreadPool(int quantityThreads) {
        this.quantityThreads = quantityThreads;
        this.threads = new Workers[quantityThreads];
        this.queue = new LinkedBlockingQueue();
    }

    public void start(){
        for (int i = 0; i < quantityThreads; i++) {
            threads[i] = new Workers();
            threads[i].start();
        }
    }

    public void execute(Runnable task) {
        synchronized (queue) {
            queue.add(task);
            queue.notifyAll();
        }
    }

    @Override
    public void interrupted() {
        for(Thread thread:threads){
            thread.interrupt();
        }
    }


    private class Workers extends Thread {
        public void run() {
            Runnable task;
            while (true) {
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace(System.out);
                        }
                    }
                    task = (Runnable) queue.poll();
                }
                try {
                    task.run();
                } catch (RuntimeException e) {
                    e.printStackTrace(System.out);
                }
            }
        }
    }
}