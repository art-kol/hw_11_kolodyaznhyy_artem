package MyThreadPool;

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

public class ScalableThreadPool implements ThreadPool{

    private int minThreads;
    private int maxThreads;
    private ArrayList<Workers> threads;
    private final LinkedBlockingQueue queue;

    public ScalableThreadPool(int minThreads, int maxThreads) {
        this.minThreads = minThreads;
        this.maxThreads = maxThreads;
        this.threads = new ArrayList<>();
        this.queue = new LinkedBlockingQueue();
    }

    public void start(){
        for (int i = 0; i < minThreads; i++) {
            threads.add(i, new Workers());
            threads.get(i).start();
        }
    }

    public void execute(Runnable task) {
        synchronized (queue) {
            queue.add(task);
            queue.notifyAll();
        }

        if(queue.size()>threads.size()){
            try {
                // Для проверки добавления новых потоков если заданий больше чем потоков
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for (int i = minThreads; i < maxThreads; i++) {
                threads.add(i, new Workers());
                threads.get(i).start();
            }
        }


    }

    @Override
    public void interrupted() {
        for (int i = 0; i < maxThreads-minThreads; i++) {
            if(!threads.get(i).isInterrupted()){
                threads.get(i).interrupt();
            }
        }
    }

    private class Workers extends Thread {
        public void run() {
            Runnable task;
            while (!Thread.currentThread().isInterrupted()) {
                synchronized (queue) {
                    if (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                        }
                    }
                    task = (Runnable) queue.poll();
                }
                try {
                    if(!queue.isEmpty()){
                        task.run();
                    }
                } catch (RuntimeException e) {
                    e.printStackTrace(System.out);
                }
            }
        }
    }
}
